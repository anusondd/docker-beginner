- Docker Image
docker images หรือ docker image ls 
->เอาไว้เรียกดู image
docker pull  image-name:tag 
->เอาไว้ดาวน์โหลด image
docker push  image-name:tag 
->เอาไว้อัพโหลด image
docker image rm -f  image-name:tag
-> เอาไว้ลบ image
docker prune image 
-> เอาไว้ลบ image ที่ไม่ได้ใช้ทิ้งไป
docker run --name container-name  -p external-port:internal-port  image-name:tag
-> เอาไว้รัน image
-> ตัวอย่าง docker run --name nginx  -p 8080:80 nginx :latest
docker image tag  image-name:old   image-name:new
-> เอาไว้เปลี่ยนชื่อ  image และ tag
-> ตัวอย่าง docker image tag  nginx :latest  nginx :pkg-1.0
docker build -t  image-name:tag  . 
-> เอาไว้สร้าง  image จาก Dockerfile

Docker Network
docker network ls 
->เอาไว้เรียกดู network
docker network rm  network-name
-> เอาไว้ลบ network
docker prune network 
-> เอาไว้ลบ network ที่ไม่ได้ใช้ทิ้งไป
docker network create --driver=network-type --subnet=subnet-ip/24 --gateway=gateway-ip network-name
-> เอาไว้สร้าง network
-> ตัวอย่าง docker network create --driver=bridge --subnet=192.168.10.0/24 --gateway=192.168.10.1 gitlab
Docker Container
docker ps  -> เรียกดู container ที่รันอยู่
docker container ls  -> เรียกดู container ทั้งหมด
docker stop container-name -> สั่งหยุดการทำงาน
docker start container-name -> สั่งเริ่มการทำงาน
docker rm container-name  -> ลบ container 
docker prune  -> ลบ container  ที่ไม่ได้รันอยู่

docker-compose up -d  -> สั่งเริ่มการทำงานจาก docker-compose.yml

docker-compose down -> สั่งหยุดการทำงานจาก docker-compose.yml